import "./LoginForm.module.css";

const LoginForm = () => {
    return(
        <>
        <form className="form">
            <label className="label" htmlFor="username">Username</label>
            <input className="input" type="text" name="username" />

            <label className="label" htmlFor="password">Password</label>
            <input className="input" type="password" name="password" />
        </form>
        <button className="submit">Click</button>
        </>
    );
}

export default LoginForm;